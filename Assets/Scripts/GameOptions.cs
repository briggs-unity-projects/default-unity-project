﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameOptions : MonoBehaviour
{
    [Header("Global Settings")]
    [InspectorName("Game Name")]
    public string gameName;
    [InspectorName("Main Font")]
    public TMP_FontAsset mainFont;
    [InspectorName("UI Font")]
    public TMP_FontAsset uiFont;

    [Header("Main Menu Settings")]
    [InspectorName("Background Color")]
    public Color backgroundColor;
    [InspectorName("Button Color")]
    public Color buttonColor;
    [InspectorName("Button Text Color")]
    public Color buttonTextColor;

}