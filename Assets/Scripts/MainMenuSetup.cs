﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuSetup : MonoBehaviour
{
    [Header("Game Options")]
    [InspectorName("Game Options")]
    [SerializeField]
    private GameOptions gameOptions;

    [Header("UI Components")]
    [InspectorName("Game Title")]
    [SerializeField]
    private TMP_Text gameTitle;
    [InspectorName("Main Menu Buttons")]
    [SerializeField]
    private List<Button> mainMenuButtons;

    // Start is called before the first frame update
    void Start()
    {
        // Game Title
        gameTitle.SetText(gameOptions.gameName);
        gameTitle.font = gameOptions.mainFont;

        // Buttons
        foreach(Button button in mainMenuButtons)
        {
            // Button text
            TMP_Text buttonText = button.GetComponentInChildren<TMP_Text>();
            buttonText.font = gameOptions.uiFont;
            buttonText.color = gameOptions.buttonTextColor;
            // Button color
            button.image.color = gameOptions.buttonColor;
        }

    }
}
