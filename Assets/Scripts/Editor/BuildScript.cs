﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEditor.Build.Reporting;
using UnityEngine.Networking;

public class BuildScript 
{

    static int BuildReport(BuildPlayerOptions buildPlayerOptions)
    {
        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
            return 0;
        }

        if (summary.result == BuildResult.Failed)
        {
            Debug.Log("Build failed");
            return -1;
        }

        return -1;
    }

    [MenuItem("Custom Utilities/Build WebGL")]
    static int PerformBuildWebGL()
    {
        Debug.Log("Starting WebGL Build");
        // Build
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scenes/MainMenu.unity" };
        buildPlayerOptions.locationPathName = "./Builds/WebGL/";
        buildPlayerOptions.target = BuildTarget.WebGL;
        buildPlayerOptions.options = BuildOptions.None;
        
        return BuildReport(buildPlayerOptions);
    }

    [MenuItem("Custom Utilities/Build Win64")]
    static int PerformBuildWin64()
    {
        Debug.Log("Starting Win64 Build");

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scenes/MainMenu.unity" };
        buildPlayerOptions.locationPathName = "./Builds/Win64/Application.exe";
        buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
        buildPlayerOptions.options = BuildOptions.None;
        
        return BuildReport(buildPlayerOptions);
    }


    [MenuItem("Custom Utilities/Build Win32")]
    static int PerformBuildWin32()
    {
        Debug.Log("Starting Win32 Build");

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scenes/MainMenu.unity" };
        buildPlayerOptions.locationPathName = "./Builds/Win64/Application.exe";
        buildPlayerOptions.target = BuildTarget.StandaloneWindows;
        buildPlayerOptions.options = BuildOptions.None;
        
        return BuildReport(buildPlayerOptions);
    }

    [MenuItem("Custom Utilities/Build Linux64")]
    static int PerformBuildLinux()
    {
        Debug.Log("Starting Linux Build");

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scenes/MainMenu.unity" };
        buildPlayerOptions.locationPathName = "./Builds/Linux/Application.x86_64";
        buildPlayerOptions.target = BuildTarget.StandaloneLinux64;
        buildPlayerOptions.options = BuildOptions.None;
        
        return BuildReport(buildPlayerOptions);
    }

    [MenuItem("Custom Utilities/Build OSX")]
    static int PerformBuildOSX()
    {
        Debug.Log("Starting OSX Build");

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scenes/MainMenu.unity" };
        buildPlayerOptions.locationPathName = "./Builds/OSX/Application";
        buildPlayerOptions.target = BuildTarget.StandaloneOSX;
        buildPlayerOptions.options = BuildOptions.None;
        
        return BuildReport(buildPlayerOptions);
    }

}